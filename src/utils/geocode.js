const request = require('request')


const geocode = (address , callback) => {
         
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ address +'.json?access_token=pk.eyJ1IjoibW9oYW1tYWQta2FiYWxhbiIsImEiOiJjbDJ0aDZmNmEwNGJ3M2ttd2huaWI2YWxoIn0.KFToXOd89dObxQCha-5Gsg'
       
    request({ 'url' : url , json :true} , (error , response) => {
            if(error)
            {
               callback('unabele to connect to location servies !' , undefined)
            }
            else if(response.body.features.length === 0 )
            {
                  callback('unable to find location , try another search', undefined) 
            }
            else
            {
                  callback(undefined , {
                        latitude   : response.body.features[0].center[1] ,
                        longtitude : response.body.features[0].center[0] ,
                        location   : response.body.features[0].place_name
                  })
            }
    })  
}
module.exports  = geocode