const path = require('path')
const express = require('express')
const req = require('express/lib/request')
const res = require('express/lib/response')
const hbs = require('hbs')

const geocode = require('./utils/geocode') 
const forecast = require('./utils/forecast')


const app = express()
const port = process.env.PORT || 3000 

// define paths for express config
const publicDirctory = path.join(__dirname,'../public')
const viewPath       = path.join(__dirname , '../template/views')
const partialsPath   = path.join(__dirname , '../template/partials')

// Setup handlebars engine and views location
app.set('view engine' , 'hbs')
app.set('views',viewPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirctory))

app.get('', (req , res) => {
    res.render('index',{
        title: 'Weahter app' ,
        name: 'Mohammad Kabalan',
    })
})

app.get('/about',(req,res) => {
    res.render('about',{
        title: 'About me' ,
        name: 'Mohammad Kabalan',
    })
})
app.get('/help',(req , res) => {
    res.render('help',{
        title: 'Help' ,
        name: 'Mohammad Kabalan',
        helpText : 'This is Some helpful text .'
    })
})


app.get('/weather' , (req , res)=> {
 
    if(!req.query.address)
    {
        return res.send({
            error : 'Must provied address with value'
        })
    }

   geocode(req.query.address ,(error , {latitude , longtitude ,location} = {} ) => {
                   
         if(error)
         {
             res.send({error})
         }

         forecast(latitude , longtitude ,(error , forecastData) => { 
             if(error)
             {
                 return res.send({ error })
             }
             res.send({
                 forecast : forecastData ,
                 location , 
                 address : req.query.address
             })
         })
   })

    // res.send({
    //     forecast : 'it is snowing',
    //     location : 'Phlidivay',
    //     address : req.query.address
    // })



})
app.get('/products', (req , res)=>{

    if(!req.query.search)
    {
       return res.send({
            error : 'must provive search with value'
        })
    }
    console.log(req.query)
    res.send({
        'products' : [],
    })
})

app.get('help/*', (req , res) => {
    res.send('Help artical not foind')
})

app.get('*', (req , res) => {
         res.render('404',{
           title : '404' ,
           name: 'Mohammad Kabalan' ,
           erorrMessage : 'Page not found'       
         })
})




app.listen(port , () => {
    console.log('Server is up on port'+port)
})